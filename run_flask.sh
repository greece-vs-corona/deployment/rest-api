#!/bin/sh

gunicorn -b 0.0.0.0:8000 app:app --workers=$FLASK_WORKERS --threads=$FLASK_THREADS --timeout 8000 --log-level debug
