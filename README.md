# REST API for accessing the services provided
REST API for accessing the services provided by the PrecMed system. For it to work properly a mongodb needs to be installed in the cluster first.


`helm repo add bitnami https://charts.bitnami.com/bitnami`

`helm install --name precmed bitnami/mongodb`

Then install the rest-api by issuing 

`kubectl apply -f k8s/rest-dp.yml`

`kubectl apply -f k8s/rest-svc.yml`

To access the services of rest api assuming you are a user first get a token with the following command:

`ACCESS_TOKEN=$(curl -d 'client_id=service-rest' -d 'client_secret=<CLIENT_SECRET>' -d 'username=<USERNAME>' -d 'password=<PASSWORD>' -d 'grant_type=password' 'https://<KEYCLOAK_URL>/auth/realms/precmed/protocol/openid-connect/token' | grep -oP '(?<="access_token":")[^"]*')`

You can find the CLIENT_SECRET of service-rest form keycloak or k8s and is the one set in installation.

Then curl the dummy /api service while providing the access token 

`curl -X GET -H "Authorization: Bearer $ACCESS_TOKEN" "https://<REST_URL>/api"`

## /launch 
At /launch with POST you can submit a workflow. 

`curl -X POST -H "Authorization: Bearer $ACCESS_TOKEN" "https://<REST_URL>/launch" -F orderId=<ORDER_ID> -F type=<WORFLOW_TYPE> -F bam=@EMQN32.bam<FILE1> -F <FILE2> ..`

The message returned is raw text with the status that Cromwell returned at the submission so either "Submitted" or an error message from Cromwell. If something goes bad in the backend then an error from the backend is returned.
  
## /metadata
At /metadata with POST you can retrieve metadata for a workflow someone from your group has submitted. 
The function requires that you supply the orderid. For example:

`curl -X POST -H "Authorization: Bearer $ACCESS_TOKEN" "https://<REST_URL>//metadata" -F orderId=<ORDER_ID>` 

The fields of metadata are the following. 

        workflowType: One of the workflow types that rest api provides.

        userId: The user id that submitted this workflow.

        username: The usernmae of that user. 

        group: The group the user belongs to.

        orderId: The order Id the user gave when submitting.

        end: When the workflow finished if it did.

        start: When Cromwell started the workflow.

        status: The status of the workflow.

        submission_date_time: When the submission happened.

        finished: Flag used by the load to know if it will process or not this entry.

        failures_dict: A json with the failures messages as they are returned by Cromwell. Internally it is stored as a dictionary.

        outputs_dict: A json with the outputs location as they are returned by Cromwell. Internally it is stored as a dictionary.

Specifically failures_dict returns a json of the following form

        {failures: [FAILURES]} where FAILURES has the following recursive schema 
        
        FAILURES = {message: (string), causedBy: [FAILURES]}

If the resource is not found (for example there is no such order id in database or the group of the submission is different from the user that made the call to the function) it returns an appropriate error message informing the user and a status of 404.

If multiple entries of this order id have been found then that is an error in the server side and it returns status of 500 with an appropriate message.


## /orderids

At /orderids with GET method a user can retrieve all order ids that someone in his group has submitted. The result comes at a json format:

        {orderIds: [(string)]}

If no submissions exist in the database for this group then the following error message is returned with status 404. 

        {"error": "This group has no submissions stored in the database yet."}
