import logging
import os
import re
import urllib.request
import urllib.parse
import uuid
import requests
from botocore import exceptions
from flask import request
from yaml import load, dump, SafeLoader
import pymongo
from routines.routine import Routine
from routines.cancel import Cancel
import datetime


class SubmitWorkflow(Routine):

    def __init__(self, handle, user_id, username, groups):

        super().__init__(user_id, groups)
        self.handle = handle
        self.workflows_tank = self.config['workflows_tank']
        self.workflows = self.config['workflows']
        self.workflow_path = None
        self.workflow_type = None
        self.workflow_status = None
        self.workflow = None
        self.uuid = str(uuid.uuid4())
        self.inputs = None
        self.input_names = {}
        self.input_bucket_name = self.config["inputs_bucket"]
        self.dir_path = "./storage/"
        self.local_inputs_path = self.dir_path + "inputs"
        if not os.path.exists(self.local_inputs_path):
            os.makedirs(self.local_inputs_path)
        self.yaml_input_path = None
        self.metadata = {}
        self.workflow_id = ""
        self.username = username
        self.workflowSource_path = ""
        self.workflowInputs_path = ""
        self.input_paths = []
        self.required_parameters = []
        self.workflowRef = {}
        self.files = {}

    def post(self):

        err = super().post()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        # Validate the existence of input type of workflow
        if 'type' not in request.form:
            err = "Error: Missing workflow type.\n"
            logging.error(err)
            return self.handle(err)

        # check if that specific workflow type exists
        self.workflow_type = request.form['type']
        if self.workflow_type not in self.workflows:
            err = "Error: No such type of workflow.\n"
            logging.error(err)
            return self.handle(err)
        self.workflow = self.workflows[self.workflow_type]

        # check if order id (provided or generated) is valid
        if not self.workflow_type == "custom":
            pattern1 = "^[0-9]{4}-[0-9]{5}-[0-9]{4}$"
            pattern2 = "^[0-9]{13}$"
            if not bool(re.match(pattern1, self.orderId)):
                if not bool(re.match(pattern2, self.orderId)) or not self.validate_date():
                    err = {
                        "error": "Referral number is invalid. It must be of the form XXXX-XXXXX-XXXX where X is any digit or YYMMDDXXXXXXX."}
                    logging.error(err)
                    return self.handle(err)
        # check if order id is taken (checks against all groups)
        available,err = self.order_id_available()
        if not available and err == "taken":
            err = "Error: Referral number already taken.\n"
            logging.error(err)
            return self.handle(err)
        if not available and err == "db":
            err = "Error: Cannot connect to workflow metadata database..\n"
            logging.error(err)
            return self.handle(err)


        self.workflow_path = self.workflows_tank + "/" + \
            self.workflow["repository"] + \
            "/raw/" + self.workflow["ref"] + "/"

        # ...................................................
        self.inputs = {}
        # If running a workflow against an older run dont check for inputs at all
        if 'workflowReference' in request.form and not request.form['workflowReference'] == "":
            err, self.workflowRef = self.retrieve_referenced_workflow(
                request.form['workflowReference'])
            if err is not None:
                logging.error(err)
                return self.handle(err)
        # Else if type not custom then use config file to find the required parameters the user should have provided
        elif not self.workflow_type == "custom":
            inputs_guide = self.workflow["inputs_parameters"]
            self.find_required_parameters(inputs_guide)
            logging.error(self.required_parameters)
            # Use the required  parameters to validate the users input in the request
            for parameter in self.required_parameters:
                if parameter not in request.files or not request.files[parameter]:
                    err = "Error: Missing " + parameter + " file.\n"
                    logging.error(err)
                    return self.handle(err)

        self.inputs = dict((k, v) for k, v in request.files.items() if v)
        # ...................................................

        try:
            for k, f in self.inputs.items():
                # handle the workflow yaml and/or inputs yaml seperately if available. Remember its local path to invoke cromwell later.
                if k == "workflowSource":
                    self.workflowSource_path = os.path.join(
                        self.local_inputs_path, self.uuid + '_' + k + '_' + f.filename)
                    f.save(self.workflowSource_path)
                elif k == "inputs_local":
                    self.workflowInputs_path = os.path.join(
                        self.local_inputs_path, self.uuid + '_' + k + '_' + f.filename)
                    f.save(self.workflowInputs_path)
                else:
                    f.save(os.path.join(self.local_inputs_path,
                                        self.uuid + '_' + k + '_' + f.filename))
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        
        # Upload input files to pmu-platform
        err = self.upload_files()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        # Build the correct yaml-workflow-input or use the preexisting one of the older referenced workflow
        err = self.build_yml_input()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        # Invoke Cromwell
        files = {}
        if self.workflow_type == "custom":
            if 'workflowUrl' in request.form and not request.form['workflowUrl'] == "":
                files = {
                    'workflowUrl': (None, request.form['workflowUrl']),
                    'workflowInputs': (self.yaml_input_path, open(self.yaml_input_path, 'rb')),
                    'workflowType': (None, self.workflow["specification_language"]),
                    'workflowTypeVersion': self.workflow["workflow_version"]
                }
            elif not self.workflowSource_path == "":
                files = {
                    'workflowSource': (self.workflowSource_path, open(self.workflowSource_path, 'rb')),
                    'workflowInputs': (self.yaml_input_path, open(self.yaml_input_path, 'rb')),
                    'workflowType': (None, self.workflow["specification_language"]),
                    'workflowTypeVersion': self.workflow["workflow_version"]
                }
            else:
                return self.handle({"error": "You did not provide any source to a cwl for custom workflow type"})
        else:
            files = {
                'workflowUrl': (None, self.workflow_path + self.workflow['cwl']),
                'workflowInputs': (self.yaml_input_path, open(self.yaml_input_path, 'rb')),
                'workflowType': (None, self.workflow["specification_language"]),
                'workflowTypeVersion': self.workflow["workflow_version"]
            }
        self.files = files

        self.save_metadata(cromwell_involved=0)
        r = requests.post(self.cromwell, files=files)
        if not r.ok:
            err = self.cromwell + " returned " + str(r.status_code)
            logging.error(err)
            return self.handle(err)
        ret = r.json()
        print(ret)
        if 'status' in ret and ret['status'] == "fail":
            err = ret['message']
            logging.error(err)
            return self.handle(err)

        # Get workflow's id and status from cromwell's response
        self.workflow_id = ret["id"]
        self.workflow_status = ret["status"]

        # Write metadata in mongodb
        self.save_metadata(cromwell_involved=1)

        # # Cleaning
        try:
            os.remove(self.yaml_input_path)
            for k, f in self.inputs.items():
                os.remove(self.local_inputs_path + "/" +
                          self.uuid + '_' + k + '_' + f.filename)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        response = {
            'status': self.workflow_status,
            'orderId': self.orderId,
            'workflowId': self.workflow_id
        }
        return self.handle(response)

    def save_metadata(self,cromwell_involved=0):
        
        """Saves metadata in mongodb"""
        with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                                 username=os.getenv('MONGODB_USER'),
                                 password=os.getenv('MONGODB_PASSWORD'),
                                 authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
            
            mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]

            metadata_documents = mongodb["metadata"]
            query = {"orderId": self.orderId}

            previous_workflows = None
            try:
                previous_workflows = metadata_documents.find(query)
            except:
                err = "Error: Cannot connect to workflow metadata database..\n"
                logging.error(err)
                return self.handle(err)

            new_entry = {}
            if 'workflowUrl' in self.files:
                with urllib.request.urlopen(self.files['workflowUrl'][1]) as f:
                    file_content = f.read().decode('utf-8')
            else:
                with open(self.files['workflowSource'][0], 'r') as f:
                    file_content = f.read()

            if(cromwell_involved):
                logging.debug("cromwell is involved now")
                new_entry['workflowId'] = self.workflow_id
                new_entry['workflowType'] = self.workflow_type
                new_entry['status'] = self.workflow_status
                try:
                    
                    metadata_documents.update_one(
                        {"orderId": self.orderId},
                            {"$set": { "workflowId" : new_entry['workflowId'],
                                        "workflowType": new_entry['workflowType'],
                                        "status": new_entry['status'] 
                                      }
                            }
                    )
                except:
                    err = "Error: Cannot connect to workflow metadata database..\n"
                    logging.error(err)
                    logging.error("Cancelling workflow")
                    params = {'id': self.workflow_id, 'version': 'v1'}
                    requests.post(self.cromwell + "/" + self.workflow_id + "/abort", params=params)
                    return self.handle(err)
            else:
                new_entry['cwl_input_file'] = file_content
                new_entry['run'] = previous_workflows.count()
                new_entry['userId'] = self.user_id
                new_entry['username'] = self.username
                new_entry['group'] = self.group
                new_entry['orderId'] = self.orderId
                new_entry['finished'] = 0
                new_entry['input_paths'] = self.input_paths
                if self.workflowRef:
                    new_entry['inputs_yaml'] = self.workflowRef['inputs_yaml']
                    new_entry['outputs_ref'] = self.workflowRef['outputs']
                else:
                    new_entry['inputs_yaml'] = self.orderId + "_inputs.yaml"

                try:
                    metadata_documents.insert_one(new_entry)
                except:
                    err = "Error: Cannot connect to workflow metadata database..\n"
                    logging.error(err)
                    return self.handle(err)


            

    def retrieve_referenced_workflow(self, orderid):
        """Retrieves metadata of a referenced workflow"""
        with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                                 username=os.getenv('MONGODB_USER'),
                                 password=os.getenv('MONGODB_PASSWORD'),
                                 authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
            mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
            metadata_documents = mongodb["metadata"]
            query = {"orderId": self.orderId, "status": "Succeeded"}
            workflows = metadata_documents.find(query)

            number_of_workflows = 0
            try:
                number_of_workflows = workflows.count()       
            except Exception as e:
                err = "Error: Cannot connect to workflow metadata database..\n"
                logging.error(err)
                return self.handle(err)
           
            if number_of_workflows == 0:
                return {"error": "There is not a successful run with this order ID for your group yet."}, None
            elif number_of_workflows > 1:
                err = "There are many succeeded runs under this order Id."
                logging.error(err)
                return {"error": err}, None
            else:
                return None, workflows[0]

    def change_host_value(self, yaml_node, match, netloc, parent):
        # Changes host value in the yaml to correct host of s3
        # Also collects all input variables of yaml and saves them as
        # [{<path in yaml of input varialbe >:<path in s3 of file>}] to self.input_paths
        # to store to metadata the location of input files and to which variables they were bound in cwl
        # e.g [ {.input_array[1].input_record.first_record_file : s3://.. } , ...]
        if type(yaml_node) == list:
            i = 0
            for item in yaml_node:
                self.change_host_value(
                    item, match, netloc, parent+"["+str(i)+"]")
                i = i+1
        else:
            for key, value in yaml_node.items():
                if type(value) == dict or type(value) == list:
                    if parent == "":
                        new_parent = key
                    else:
                        new_parent = parent+"["+key+"]"
                    self.change_host_value(
                        value, match, netloc, new_parent)
                elif key == match:
                    path = yaml_node[key]
                    parsed_path_list = list(urllib.parse.urlparse(path))
                    parsed_path_list[1] = netloc
                    yaml_node[key] = urllib.parse.urlunparse(parsed_path_list)
                    self.input_paths.append({parent: yaml_node[key]})

    def fix_paths(self, inputs_yaml, guide):
        # For predefined workflows it
        # fixes paths of file variables in yaml that were provided by the user to be
        # the correct ones they were saves in s3
        path = "s3://" + self.netloc + "/" + self.input_bucket_name + "/"

        if type(guide) == list:
            i = 0
            for item in guide:
                self.fix_paths(inputs_yaml[i], item)
                i = i+1
        else:
            for key, value in guide.items():
                if type(value) == dict or type(value) == list:
                    self.fix_paths(inputs_yaml[key], guide[key])
                elif value == "required":
                    inputs_yaml['path'] = path + self.input_names[key]
                elif value == "optional" and key in self.input_names:
                    inputs_yaml['path'] = path + self.input_names[key]

    def find_required_parameters(self, guide):
        # collects all required input files from the config of the specific predefined workflow
        if type(guide) == list:
            for item in guide:
                self.find_required_parameters(item)
        else:
            for key, value in guide.items():
                if type(value) == dict or type(value) == list:
                    self.find_required_parameters(value)
                elif value == "required":
                    self.required_parameters.append(key)

    def build_yml_input(self):
        """
        Retrieves the yml file from 'workflow_yml' location,
        or from the user request, or from the s3.
        De-serializes it, adds the correct input paths (if needed),
        then serializes it again and returns it

        If workflowRef is not empty retrieves that specific workflow inputs yaml unchanged.
        """

        if self.workflowRef:
            inputs_s3_path = self.workflowRef['inputs_yaml']
            err, local_file_path = self.download_file(inputs_s3_path)
            self.yaml_input_path = local_file_path
            if err is not None:
                logging.error(err)
                return err
            return None

        path = "s3://" + self.netloc + "/" + self.input_bucket_name + "/"

        workflow_yml = self.workflow['yml']

        try:
            data = {}
            # if workflow is custom then get the inputs yaml from the request, or download it /get it from s3
            if self.workflow_type == "custom":
                if not self.workflowInputs_path == "":
                    with open(self.workflowInputs_path, 'rb') as file:
                        data = load(file, Loader=SafeLoader)

                elif 'inputs_url' in request.form and not request.form['inputs_url'] == "":
                    inputs_url = request.form['inputs_url']
                    r = requests.get(inputs_url)
                    if not r.ok:
                        err = "Trying to reach inputs url: " + \
                            inputs_url + " returned: " + str(r.status_code)
                        logging.error(err)
                        raise Exception(err)
                    read = r.content
                    data = load(read, Loader=SafeLoader)

                elif 'inputs_s3' in request.form and not request.form['inputs_s3'] == "":
                    # the path of file that is in workflow-inputs bucket in s3
                    inputs_s3_path = request.form['inputs_s3']
                    err, local_file_path = self.download_file(inputs_s3_path)
                    if err is not None:
                        raise Exception(err)
                    else:
                        with open(local_file_path, 'rb') as file:
                            data = load(file, Loader=SafeLoader)
                else:
                    err = {
                        "error": "You did not provide a source for inputs yaml for custom workflow."
                    }
                    return err
            else:
                # if workflow is predefined retrieve it from the predefined repository that is defined in config
                r = requests.get(self.workflow_path + workflow_yml)
                read = r.content
                data = load(read, Loader=SafeLoader)
                logging.error(data)

            # if type is custom workflow for every input file parameter in request find the entry in inputs yaml and
            # update it with the correct path the file has in s3.
            if self.workflow_type == "custom":
                # extract the full location in inputs yaml from the actual parameter name
                for name, location in self.input_names.items():
                    x = re.split(r'\[|\]', name)
                    for key in x:
                        logging.error(key)
                    if x[0] in data:
                        iter_data = data
                        for key in x:
                            if type(iter_data) == list:
                                if not key == "" and int(key) < len(iter_data):
                                    iter_data = iter_data[int(key)]
                            else:
                                if not key == "" and key in iter_data:
                                    iter_data = iter_data[key]
                        iter_data['path'] = path + location
            else:
                # use the config to find what paths need to change
                self.fix_paths(data, self.workflow["inputs_parameters"])

            # change every istance of host in every path with the current implementation host (taken from os env)
            self.change_host_value(data, "path", self.netloc, "")

            # save the updated yaml locally and in s3 and remember the local path
            self.yaml_input_path = self.dir_path + self.uuid + "_" + workflow_yml
            with open(self.yaml_input_path, 'w+') as write:
                dump(data, write)
            try:
                bucket = self.client.Bucket(self.input_bucket_name)
                bucket.upload_file(
                    Filename=self.yaml_input_path, Key=self.orderId+"_inputs.yaml")

            except exceptions.ClientError as err:
                logging.error(err)
                return self.handle(err)

        except Exception as err:
            logging.error(err)
            if not isinstance(err, str) and not isinstance(err, dict):
                err = "Oops something went wrong. Try again later!\n"
            return self.handle(err)

        return None

    def upload_files(self):
        """
            Uploads input files to s3
        """
        # prefix = ""
        for k, f in self.inputs.items():

            file_name = self.uuid + '_' + k + '_' + f.filename
            altered_file_name = file_name.replace('[', '_').replace(']', '_')
            self.input_names[k] = altered_file_name

            try:
                bucket = self.client.Bucket(self.input_bucket_name)
                bucket.upload_file(Filename=os.path.join(
                    self.local_inputs_path, file_name), Key=altered_file_name)

            except exceptions.ClientError as err:
                logging.error(err)
                return self.handle(err)
        return None

    def download_file(self, path):
        """
            Downloads a file from s3 input bucket
        """
        try:
            bucket = self.client.Bucket(self.input_bucket_name)
            obj = bucket.Object(path)
            download_file_path = os.path.join(
                self.local_inputs_path, self.uuid + "_inputs.yaml")
            if obj is not None:
                bucket.download_file(Filename=download_file_path, Key=path)
                return None, download_file_path
        except exceptions.ClientError as err:
            return err, None
        return None, None

    def order_id_available(self):
        with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                                 username=os.getenv('MONGODB_USER'),
                                 password=os.getenv('MONGODB_PASSWORD'),
                                 authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
            mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
            metadata_documents = mongodb["metadata"]
            statuses = ["Running", "Submitted", "Succeeded"]
            query = {"orderId": self.orderId, "status": {"$in": statuses}}
            workflows = metadata_documents.find(query)
            
            number_of_workflows = 0
            try:
                number_of_workflows = workflows.count()       
            except Exception as e:
                logging.error(e)
                return False,"db"
           
            if number_of_workflows > 0:
                return False,"taken" 
            else:
                return True,""      
            

    def validate_date(self):

        try:
            datetime.datetime.strptime(
                "20" + self.orderId[0:2] + "-" + self.orderId[2:4] + "-" + self.orderId[4:6], '%Y-%m-%d')
            return True
        except ValueError:
            return False
