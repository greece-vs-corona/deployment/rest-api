from flask import request
from routines.routine import Routine
from datetime import datetime
import subprocess
import botocore
import logging
import os
import pymongo
import os
import shutil
import glob
from zipfile import ZipFile
from os.path import basename


class Outputs(Routine):
    """
         Asks Mongo about workflow run output locations;
         Downloads the output files from these locations
         Returns files to user
    """

    def __init__(self, handle, user_id, groups):

        super().__init__(user_id, groups)
        self.handle = handle
        self.workflow_id = None
        self.path = None
        self.dir_path_outputs = "./storage/outputs/"
        if not os.path.exists(self.dir_path_outputs):
            os.makedirs(self.dir_path_outputs)

    def post(self):

        err = super().post()
        if err is not None:
            logging.error(err)
            return self.handle(err)
        with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                                 username=os.getenv('MONGODB_USER'),
                                 password=os.getenv('MONGODB_PASSWORD'),
                                 authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
            mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
            metadata_documents = mongodb["metadata"]
            query = {"orderId": self.orderId, "status": "Succeeded"}
            workflows = metadata_documents.find(query)

            number_of_workflows = 0
            try:
                number_of_workflows = workflows.count()   
            except Exception as e:
                logging.error(e)
                return {"message": "Cannot connect to workflow metadata database."}      
           
            if number_of_workflows == 0:
                return {"error": "There is not a submission with this order ID or you don't have the rights to access it."}, 404
            elif number_of_workflows > 1:
                logging.error("There are many results in database for order id: " +
                              request.form['orderId']+" and group: "+self.group+".")
                return {"error": "There are many successful runs in database for order id: "
                        + request.form['orderId']+" and group: "+self.group+"."}, 500
            else:
                workflow = workflows[0]
            now = datetime.now()
            timestamp = datetime.timestamp(now)
            self.path = self.dir_path_outputs + \
                self.orderId + "/" + str(timestamp) + "/"
            outputs = workflow['outputs']
            for output_name in outputs:
                err = self.download_file(outputs[output_name]['location'])
                if err is not None:
                    logging.error(err)
                    return self.handle(err)
    

        zipName = os.path.join(self.path,"outputs.zip")
        with ZipFile(os.path.join(self.path,"outputs.zip") ,'w') as zipObj:
            for folderName, subfolders, filenames in os.walk(self.path):
                for filename in filenames:
                    if filename != "outputs.zip":
                        filePath = os.path.join(folderName, filename)
                        zipObj.write(filePath, basename(filePath))
                        os.unlink(os.path.join(self.path,filename))
            
            return self.path, zipName

    


    def download_file(self, location):

        x = location.split("/", 4)
        bucket_name = x[3]
        obj_name = x[4]
        file_name = x[4].split("/")
        try:
            bucket = self.client.Bucket(bucket_name)
            obj = bucket.Object(obj_name)
            if obj is not None:
                if not os.path.exists(self.path):
                    os.makedirs(self.path)
                path = self.path + self.orderId + \
                    "." + file_name[-1].split(".", 2)[0] + \
                    "."+file_name[-1].split(".", 2)[1]
                bucket.download_file(Filename=path, Key=obj_name)
        except botocore.exceptions.ClientError as err:
            return err
        return None
