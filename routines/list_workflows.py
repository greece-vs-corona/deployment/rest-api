from flask_restful import Resource
import json


class ListWorkflows(Resource):

    def __init__(self):
        with open("./config.json", "r") as fp:
            self.config = json.load(fp=fp)

    def get(self):
        workflows = self.config['workflows']
        workflows_list = []
        for workflow in workflows:
            workflows_list.append(workflow)
        return {'workflows': workflows_list}
