from routines.routine import Routine
from flask import request
import requests
import logging
import os
import pymongo


class Cancel(Routine):

    def __init__(self, handle, user_id, groups):
        super().__init__(user_id, groups)
        self.handle = handle
        self.output_bucket_name = self.config["outputs_bucket"]

    def post(self):
        err = super().post()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                                 username=os.getenv('MONGODB_USER'),
                                 password=os.getenv('MONGODB_PASSWORD'),
                                 authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
            mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
            metadata_documents = mongodb["metadata"]
            statuses = ["Running", "Submitted"]
            query = {"orderId": request.form['orderId'],
                     "group": self.group, "status": {"$in": statuses}}
            workflows = metadata_documents.find(query)
            
            number_of_workflows = 0
            try:
                number_of_workflows = workflows.count()   
            except Exception as e:
                logging.error(e)
                return {"message": "Cannot connect to workflow metadata database."}      
            
            if number_of_workflows == 0:
                return {"error": "There is not a submission with this order ID that is in status Running or Submitted."}, 404
            else:
                response = {}
                for workflow in workflows:
                    params = {'id': workflow['workflowId'], 'version': 'v1'}
                    r = requests.post(self.cromwell + "/" +
                                      workflow['workflowId'] + "/abort", params=params)
                    response['run_' + str(workflow['run'])] = r.json()
                return response
