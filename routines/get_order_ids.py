from routines.routine import Routine
import pymongo
import os
import logging


class GetOrderIds(Routine):
    def __init__(self, handle, user_id, groups):
        super().__init__(user_id, groups)
        self.handle = handle

    def post(self):
        # check mongo and get the document that corresponds to the order id in request and the user group.
        with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                                 username=os.getenv('MONGODB_USER'),
                                 password=os.getenv('MONGODB_PASSWORD'),
                                 authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
            mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
            metadata_documents = mongodb["metadata"]
            query = {"group": self.group}
            workflows = metadata_documents.find(query)
            results = []
            
            number_of_workflows = 0
            try:
                number_of_workflows = workflows.count()   
            except Exception as e:
                logging.error(e)
                return {"message": "Cannot connect to workflow metadata database."}      
            
            if number_of_workflows == 0:            
                return {"message": "No submissions for group "+self.group+" yet."}
            else:
                for workflow in workflows:
                    if 'submission_date_time' not in workflow:
                        workflow['submission_date_time'] = "null"
                    if 'end' not in workflow:
                        workflow['end'] = "null"
                    workflow['_id'] = str(workflow['_id'])
                    results.append({"id": workflow['orderId'], "type": workflow['workflowType'], "submitter": workflow['username'],
                                    "submission_datetime": workflow['submission_date_time'], "endtime": workflow['end'], "status": workflow['status'], "run": workflow['run']})
            return {"results": results}
